from tkinter import *
import math
import time
import random

class FourierVis(Frame):
    def __init__(self, parent, *args, **kwargs):
        Frame.__init__(self, parent, *args, **kwargs)
        self.parent = parent

        self.cnv = Canvas(window, bg='#333333', bd=0)
        self.cnv.pack(fill=BOTH, expand=1)
        self.cnv.bind('<Configure>', self.adjustFigure)

        self.circs = []

    def fourier(self, i):
        n = i+1

        s = n
        if n%2 == 1:
            r = self.startR * (2 / (n*-math.pi))
        else:
            r = self.startR * (2 / (n*math.pi))

        return s, r

    def adjustFigure(self, e):
        self.width = e.width
        self.height = e.height

        self.startX = int(self.width/4)
        self.startY = int(self.height/2)
        self.graphStart = int(self.width/2)
        self.graphEnd = int(self.width)

        self.cnv.delete('graph')

        if len(self.circs) > 0:
            root = self.circs[0]
            root.pos = (self.startX, self.startY)

            newBounds = root.getBounds()
            self.cnv.coords(self.circs[0].circle, newBounds)

    def startLoop(self, iters=5):
        self.t = 0
        self.startR = 75

        for i in range(iters):
            n, r = self.getProperties(i)

            if i == 0:
                self.circs.append(FourierCirc(self.startX, self.startY, r, 0, n))
            else:
                point = self.circs[i-1].getPoint(self.t)
                self.circs.append(FourierCirc(point[0], point[1], r, 0, n))

            circ = self.circs[i]
            circ.circle = self.cnv.create_oval(circ.getBounds(), outline='white')
            circ.pointer = self.cnv.create_line(circ.pos, circ.getPoint(self.t), fill='white')

        self.point = self.circs[-1].getPoint(self.t)

        self.update()

    def update(self):
        self.t += 0.05
        self.cnv.delete('offscreenPoint')

        prevPoint = self.point

        for circNum in range(len(self.circs)):
            circ = self.circs[circNum]

            if circNum > 0:
                parentCircle = self.circs[circNum-1]
                circ.pos = parentCircle.getPoint(self.t)

                self.cnv.coords(circ.circle, circ.getBounds())

            self.cnv.coords(circ.pointer, circ.pos[0], circ.pos[1],
                            circ.getPoint(self.t)[0], circ.getPoint(self.t)[1])


        self.point = self.circs[-1].getPoint(self.t)

        #Creates and moves the graph
        self.cnv.move('graph', 1, 0)
        self.cnv.create_line(self.graphStart, self.point[1], self.graphStart+1, prevPoint[1], fill='white', tags='graph')
        self.cnv.addtag_enclosed('offscreenPoint', self.graphEnd, 0, self.graphEnd+99, self.height)

        self.after(10, self.update)

class FourierCirc:
    def __init__(self, x, y, r, a, s):
        self.pos = (x, y)
        self.angle = a
        self.speed = s
        self.radius = r

        self.circle=None
        self.pointer=None

    def getPoint(self, t):
        x = self.radius * math.cos(self.speed*t)
        y = self.radius * math.sin(self.speed*t)

        return x+self.pos[0], y+self.pos[1]

    def getBounds(self):

        return self.pos[0]-self.radius, self.pos[1]-self.radius,\
               self.pos[0]+self.radius, self.pos[1]+self.radius

window = Tk()
window.title('')
window.geometry('800x400')
fourier = FourierVis(window)
window.after(1000, fourier.startLoop, 30)
window.mainloop()
